class Module {
    constructor(structure = {
        template: ``,
        components: []
    },
        baseContainer) {

        //this.selector = `[data-module="${structure.moduleName}"]`;
        //components = ['clock', 'greet'];
        //console.log(this.selector);
        this.container = baseContainer;
        this.container.innerHTML = structure.template;
        if (this.container && structure.components) {
            for (const comp in structure.components) {
                console.log(structure.components[comp]);
                var className = structure.components[comp];
                className = className.charAt(0).toUpperCase() + className.slice(1);
                var list = this.container.querySelectorAll(`[data-component="${structure.components[comp]}"]`);
                if (list) {
                    list.forEach((comp) => {
                        try {
                            var c = new window[className](comp);
                        } catch (e) {
                            console.log(e);
                        }
                    });
                }
            }
        }
    }
}
