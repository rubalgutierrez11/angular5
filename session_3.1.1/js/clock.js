var Clock = class Clock extends Component {
    constructor(componentContainer) {
        super({
            //se define el selector para encontrar elelemento dentro de index.html (data-component="clock")
            // selector: "clock",
            container: componentContainer,
            //se define el template que esta usando clock, este contiene la UI del componente.
            //Los elementos con los atributos data-value estaran ligados con los valores de data.
            //Los elementos con data-event-estan ligados a los metodos de methods.
            template: `
                        <section class="display">
                            <span class="hours" data-value="hours">0</span> :
                            <span class="minutes" data-value="minutes">0</span> :
                            <span class="seconds" data-value="seconds">0</span>
                        </section>
                        <section class="controls">
                            <button data-event-click="startClock">Start</button>
                            <button data-event-click="pauseClock">Pause</button>
                            <button data-event-click="stopClock">Stop</button>
                        </section>
                    `,
            data: {//valores iniciales de data
                seconds: 0,
                minutes: 0,
                hours: 0
            },
            //Template contiene elementos con el atributo data-event-, asi que se definen los metodos que ejecutan al dispararse los eventos.
            methods: {
                //update clock estara actualizando data y reiniciando segundos y minutos
                updateClock() {
                    if (this.data.seconds < 59) {
                        this.data.seconds++;
                    } else {
                        this.data.seconds = 0;
                        this.data.minutes++;
                    }
                    if (this.data.minutes >= 60) {
                        this.data.minutes = 0;
                        this.data.hours++;
                    }
                },
                //inicia un timer para que se actualiza el valor de data cada 100 milisegundos
                startClock() {
                    if (!this.data.clockId) {
                        this.data.clockId = setInterval(function () {
                            this.methods.updateClock();
                        }.bind(this), 100);
                    }

                },
                //borra el timer
                pauseClock() {
                    clearInterval(this.data.clockId)
                    this.data.clockId = 0;
                },
                //borra el timer e inizializa data
                stopClock() {
                    clearInterval(this.data.clockId)
                    this.data.seconds = 0;
                    this.data.minutes = 0;
                    this.data.hours = 0;
                    this.data.clockId = 0;
                }
            }
        });
    }
};
