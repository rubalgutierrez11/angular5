var router = new Router({
    '': {
        template: `
        <article class="widget" data-component="clock"></article>      
        <article class="widget" data-component="greet"></article>
        <article class="widget" data-component="contact"></article>`,
        components: ['clock', 'greet','contact']
    },
    'clock': {
        template: `
        <article class="widget" data-component="clock"></article>`,
        components: ['clock']
    },
    'greet': {
        template: `        
        <article class="widget" data-component="greet"></article>`,
        components: ['greet']
    },
    'contact': {
        template: ` 
        <article class="widget" data-component="contact"></article>`,
        components: ['contact']
    },
}
);

//var m = new Module({moduleName:`app`, components:['clock','greet']});
/*
cual es la definicion de hoisting: es mover las declaraciones de js hasta arriba.

diferencias entre let var y const: const no se puede reasignar una variable, let se usa cuando debo reasignar variables y var es simple definicion de una variable en js.

los operadores nuevos en es6

un objeto no modificable

try catch leo los componenets que estan se cea uno y se elimina component


*/