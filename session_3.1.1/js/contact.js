//contact es un objeto que extiende de Component
var Contact = class Contact extends Component {
    constructor(componentContainer) {
        //su constructor llama al constructor del padre en este caso component para inicializarlo 
        //y hacer el mount
        super({            
            container: componentContainer,            
            template: `
                  
                       
                    <form action="/">
                    
                            <label for="Nombre">Nombre</label>
                            <input type="text" name="Nombre">
                            
                            <label for="Correo">Correo</label>
                                <input type="email" name="Email">
                            
                            <label for="Comments">Comments</label>
                            <input type="text" name="Comments">
                            <button type="submit" value="Submit">Enviar</button>
                            <button>Cancel</button>
                        </form>`,
            data: { },//valores iniciales de data
            methods: {}//este no contiene controles que disparen un evento asi que se le pone vacio.
        });
    }
};
