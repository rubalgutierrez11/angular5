//greet es un objeto que extiende de Component
var Greet = class Greet extends Component {
    constructor(componentContainer) {
        //su constructor llama al constructor del padre en este caso component para inicializarlo 
        //y hacer el mount
        super({
            //se define el selector para encontrar elelemento dentro de index.html (data-component="greet")
            // selector: "greet",
            container: componentContainer,
            //Se define el template que esta usando greet, este contiene la UI del componente.
            //Los elementos con los atributos data-value estaran ligados con los valores de data.
            //Los elementos con data-event-estan ligados a los metodos de methods.
            template: `                   
                        <input type="text" data-value="name">
                        <p>Hello my name is <span data-value="name"></span></p>
                        <input type="text" data-value="lastname">
                        <p>And my name is <span data-value="lastname"></span></p>
                        <label for="chkBox">Enviar Spam?</label>
                        <input name="chkBox" type="checkbox" data-event-click="chkBoxEvent" data-value="flag">
                        <span data-value="flag"></span>`,
            data: { name: "Ruben", lastname: "", flag: "" },//valores iniciales de data
            methods: {
                chkBoxEvent() {                    
                    alert("Spam! Spam!!");
                }
            }//este no contiene controles que disparen un evento asi que se le pone vacio.
        });
    }
};
