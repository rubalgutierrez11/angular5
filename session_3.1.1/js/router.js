class Router {
    constructor(structure) {
        this._structure = structure;
        this.container = document.querySelector(`[data-router]`);
        this.changeRoute();
        addEventListener('hashchange', this.changeRoute.bind(this));

    }
    changeRoute() {
        const key = window.location.hash.slice(1);
        if (this._structure.hasOwnProperty(key)) {
            var r = new Module(this._structure[key], this.container);
        }
        else {
            window.location.href = 'page404.html';
        }
    }
}