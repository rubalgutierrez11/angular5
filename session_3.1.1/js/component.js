//Components contiene lo necesario para poder generar controles y comportamientos automaticamente
class Component {
    //Se inicializa el constructor y el parametro structure se llena con un valor por default,
    //para que siempre este inicializada.
    constructor(structure = {
        container: document,
        template: '',
        data: {},
        methods: {}
    }) {
        // Se usa structure para llenar los atributos de Component
        try {
            //this.selector = `[data-component="${structure.selector}"]`;//Se define el selector que se usara para buscar en el DOM el componente


            this.container = structure.container;//se busca el elemento usando el selector definido


            //this.container = document.querySelector(this.selector);//se busca el elemento usando el selector definido
            this.template = structure.template;//se pasa el template
            this.methods = structure.methods;//los metodos
            this.data = this.buildData(structure.data);//se crea un nuevo data que convertira los atributos del componenete en propiedades (setters y getters)
            this.mount();//monta lso controles , valores y eventos en el DOM
        } catch (e) {
            //catch el error en caso de que haya algun error al momento de montar structure o hacer sus propiedades.
            console.log(e)
        }
    }
    //Build data genera un data nuevo con getters y setters usando de referencia los valores de data.
    buildData(data = {}) {
        var computedInitalData = {}
        this._data = data;//guarda en una privada para que no se modifique el data original
        for (const propertyName in data) {//itera por cada elemento en data asi creara una propiedad por elemento
            if (data.hasOwnProperty(propertyName)) {
                Object.defineProperty(computedInitalData, propertyName, {//crea la nueva propiedad en computedInitalData
                    set: function (x) {
                        this._data[propertyName] = x;//guarda el value en data original
                        var toChange = this.container.querySelectorAll(`[data-value="${propertyName}"]`);
                        if (toChange) {//si encuentra el control entonces llama a render para que lo pinte con el nuevo valor
                            this.render(toChange, data[propertyName])
                        }
                    }.bind(this),//bindea a este objeto para que no haya problemas de scope
                    get: () => (this._data[propertyName])//solo obten el valor del data original
                });
            }
        }
        return computedInitalData;//regresa el nuevo data con propiedades, el set que hace render del nuevo valor y un get regresa el valor
    }



    //pinta los controles
    render(element = this.container, content = this.template) {//se inicializa por default en el selector del objeto y el template

        if (Node.prototype.isPrototypeOf(element)) {//si el elemento existe dentro entonces vamos montarle el valor
            if (element.tagName == "INPUT") {
                element.value = content;//si es input usa value
            } else {
                element.innerHTML = content;//si no es input entonces solo remplaza el contenido del html para mostrar el control
            }

        }
        if (NodeList.prototype.isPrototypeOf(element)) {//en caso de que sea una lista itera por cada una para renderear todos los elementos encontrados
            element.forEach((child) => {
                this.render(child, content)
            });
        }
    }
    mount() {
        //Primero pinta el template en el contenedor
        this.render(this.container, this.template);

        //itera las propiedades de data para poder montar los valores en los controles del template
        for (const key in this._data) {
            if (this.data.hasOwnProperty(key)) {
                var toChange = this.container.querySelectorAll(`[data-value="${key}"]`);//busca el elemento que le queires montar el valor de la propiedadad de data
                if (toChange) {//si lo encuentra entonces pintalo con render usando valores iniciales de data
                    this.render(toChange, this.data[key]);
                    //itera por cada elemento para checar en caso de que sean input o checkbox agregarles sus event listeners
                    toChange.forEach(element => {
                        if (element.tagName == 'INPUT') {
                            element.addEventListener('input', event => {
                                this.data[key] = event.target.value;
                            })
                            if (element.getAttribute('type') == 'checkbox') {
                                element.addEventListener('click', (click) => {
                                    this.data[key] = click.target.checked
                                })
                            }
                        }
                    });
                }
            }
        }
        //siguen agregar los eventos
        const events = ['click']//se inicializa con un evento click
        const DOMElements = events.map(event => {//se crea una structure con todos eventos que tenga el component y sus controles que diparan el evento
            return {
                event: event,
                targets: this.container.querySelectorAll(`[data-event-${event}]`)//la forma de encontrar todos los eventos es usando el prefijo data-event- mas el evento en este caso "Click"
            };
        });
        for (const method in this.methods) {//se bindean los methodos al componente para que no haya problemas de scope
            if (this.methods.hasOwnProperty(method)) {
                this.methods[method] = this.methods[method].bind(this);//para accear al metodo este debe coicidir en el control y el nombre actual del metodo. 
            }
        }
        //Por cada elemento que se encontro con el prefijo de data-event- anteriormente, se le va a agregar agrega su listener correspondiente, este caso solo es click
        DOMElements.forEach(element => {
            element.targets.forEach(target => {
                target.addEventListener(element.event, this.methods[target.getAttribute('data-event-' + element.event)]);
            });
        });
    }
}