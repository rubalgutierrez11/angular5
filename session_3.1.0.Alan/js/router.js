var Router = class Router {
    constructor(routes = {},
        currentRoute = window.location.hash) {
        this.routes = routes;
        this.container=document.querySelector('[data-router]');
        this.loadRoute();
        addEventListener('hashchange', this.changeRoute.bind(this));
    }
    changeRoute() {
        this.loadRoute();
    }
    loadRoute(route = window.location.hash.slice(1)) {
        if(this.routes[route]){
            this.container.innerHTML=this.routes[route].template;
            this.currentModule = new Module(this.routes[route],this.container);
        }
        else{
            this.currentModule='';
            document.body.innerHTML="404 page not found";
        }
    }
}