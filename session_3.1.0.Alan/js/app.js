var router = new Router({
         '': {            
            template:`
            <h1>Home</h1>
            <article data-component="clock"></article>            
            <article data-component="greet"></article>`,
            components: ['clock', 'greet']
        },
         'clock':{            
            template:`
            <h1>clock</h1>            
            <article data-component="clock"></article>`,
            components: ['clock']
        },
        'greet':{            
            template:`
            <h1>greet</h1>            
            <article data-component="greet"></article>`,
            components: ['greet']
        },
        'contactus':{            
            template:`
            <h1>Contact Us</h1>            
            <article data-component="greet"></article>`,
            components: ['greet']
        }
    })