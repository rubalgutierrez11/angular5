class Module {
    constructor(structure = {
        name: 'app',
        components: [],
    },container=document.querySelector(`[data-module="${structure.selector}"`)){        
        this.container=structure.container;
        this.components={};
        container.querySelectorAll('[data-component]').forEach((node,index)=>{
            if(Node.prototype.isPrototypeOf(node)){
                var componentName=node.getAttribute('data-component');
                if(structure.components.indexOf(componentName)>=0){
                 this.components[componentName+index] = new window[componentName.charAt(0).toUpperCase() + componentName.slice(1)](node);
                }
            }                
        })                                        
    }
}