'use strict';

angular.module('myApp.clock', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/clock', {
    templateUrl: 'clock/clock.html',
    controller: 'ClockCtrl',
    controllerAs:'vm'
  });
}])

.controller('ClockCtrl', ['$interval', function($interval) {
  
  var vm = this;

  vm.seconds=0;
  vm.minutes=0;
  vm.hours=0;
  vm.clockId=0;
 
  vm.updateClock = function() {
    if (vm.seconds < 59) {
      vm.seconds++;
    } else {
      vm.seconds = 0;
      vm.minutes++;
    }
    if (vm.minutes >= 60) {
      vm.minutes = 0;
      vm.hours++;
    }
};

vm.startClock = function() {
  
 
    if (!vm.clockId) {
      vm.clockId = setInterval(function () {
        vm.updateClock();
        })
    };

};
vm.pauseClock = function() {
    clearInterval(vm.clockId)
    vm.clockId = 0;
};
this.stopClock= function() {
    clearInterval(vm.clockId)
    vm.seconds = 0;
    vm.minutes = 0;
    vm.hours = 0;
    vm.clockId = 0;
};

}]);
