import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import Main from './Main.vue'
import Ruta1 from './Ruta1.vue'
import Ruta2 from './Ruta2.vue'

Vue.use(Router)

var router = new Router({
  routes: [
    {
    path: '/',
    name: 'Main',
    component: Main
  },
  {
    path: '/Ruta1/:show',
    name: 'Ruta1',
    component: Ruta1
  }
  ,
  {
    path: '/Ruta2',
    name: 'Ruta2',
    component: Ruta2
  }
]

})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
