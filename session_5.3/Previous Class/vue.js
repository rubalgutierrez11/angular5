const Foo = {
    template: "<p>I pity the fool!</p>"
  }
  
  const User = {
    template: '<section>User {{ $route.params.id }}</section>'
  }
  
  const routes = [{
      path: '/foo',
      component: Foo
    }
  ]
  
  const router = new VueRouter({
    routes
  })
  
  var vm = new Vue({
    el: '#app',
    router
  })