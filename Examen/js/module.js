class Module {
    constructor(structure = {
        moduleName: ``,
        components: []
    }) {

        this.selector = `[data-module="${structure.moduleName}"]`;
        //components = ['clock', 'greet'];
        console.log(this.selector);
         this.container = document.querySelector(this.selector);
        if (this.container && structure.components) {
            for (const comp in structure.components) {
                console.log(structure.components[comp]);
                var className = structure.components[comp];
                className = className.charAt(0).toUpperCase() + className.slice(1);
                var list = this.container.querySelectorAll(`[data-component="${structure.components[comp]}"]`);
                if (list) {
                    list.forEach((comp) => {
                        try {
                            var c = new window[className](comp);
                        } catch (e) {
                            console.log(e);
                        }
                    });
                }
            }
        }
    }
}
