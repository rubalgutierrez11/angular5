var angularCalc = (function () {
    'use strict';
    angular.module('myApp', [])
        .service('myCalculatorService', function () {
            this.sum = function (a, b) {
                return a + b;
            };

            this.min = function (a, b) {
                return a - b;
            };
        });
});

