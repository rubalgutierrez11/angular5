# Angular & VUE 

Este curso es un entrenamiento en Angular proporcionado por STK, aqui se listaran las practicas que se tomaran en el curso.

## Folder Examen
Este folder contiene el examen: Poner Comentarios al codigo de Module, Component, Greet y Clock.

## Folder session_1
Este folder contiene las practicas: Client, Ecriptador del Cesar y la wiki del curso de HTML5

## Folder session_2.1
Este folder contiene las practicas: Classes en JS y es6 features.

## Folder session_2.2
Este folder contiene las practicas: Avance Clock y Greet usando Components.

## Folder session_2.3
Este folder contiene las practicas: Version final de Component, Module, Router.

## Folder session_3.1
Este folder contiene las practicas: Codigo compartido por Alan de la plactica de Router.

## Folder session_3.1.1
Este folder contiene las practicas: Practica de router implementando flex.

## Folder session_4.1
Este folder contiene las practicas: Introduccion a angular usando angular-seed.

## Folder session_4.2
Este folder contiene las practicas: Conversion de las practicas de Clock y Greet a angular.

