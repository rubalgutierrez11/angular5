(function (angular) {
    'use strict';

    angular
        .module('myApp.services')
        .factory('moviesService', Definition);

    Definition.$inject = ['$q', '$http'];

    function Definition($q, $http) {
        var apiKey = '31690548';
        return {
            //getPage: function (idx, textToSearch) {no paging for now
            getPage: function (textToSearch) {
                // idx = idx || 1;no paging for now
                return $q(function (resolve) {
                    $http
                        .get(`https://www.omdbapi.com/?apikey=${apiKey}&s=${textToSearch}`)
                        .then(function (response) {
                            resolve(response.data.Search.map(function (item) {
                                return {
                                    title: item.Title,
                                    year: item.Year,
                                    poster: item.Poster
                                };
                            }));
                        });
                });
            }
        }
    };
})(angular)


