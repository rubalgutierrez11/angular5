(function (angular) {
    'use strict';

    angular
        .module('myApp.services')
        .factory('userService', Definition);

    // Definition.$inject = [];

    function Definition() {
        var vm = this;
        vm.name = '';

        return {
            login: function (userName, pass) {
                if (pass == 'pass') {
                    vm.name = userName;
                }
                else {
                    vm.name = '';
                }
                return vm.name != '';
            },
            logOut: function () {
                vm.name = '';                
            },
            isLogged: function () {                
                return vm.name != '';
            },
            getName: function () {
                return vm.name;
            }
        }
    };
})(angular)


