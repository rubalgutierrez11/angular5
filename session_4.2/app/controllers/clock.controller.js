(function (angular) {
    'use strict';

    angular
        .module('myApp.controllers')
        .controller('clockController', Definition);

    Definition.$inject = ['$interval', 'userService'];

    function Definition($interval, userService) {
        var vm = this;

        vm.seconds = 0;
        vm.minutes = 0;
        vm.hours = 0;
        vm.clockId = 0;

        vm.updateClock = function () {
            if (vm.seconds < 59) {
                vm.seconds++;
            } else {
                vm.seconds = 0;
                vm.minutes++;
            }
            if (vm.minutes >= 60) {
                vm.minutes = 0;
                vm.hours++;
            }
        };

        vm.startClock = function () {
            if (!vm.clockId) {
                vm.clockId = $interval(function () { vm.updateClock(); }, 100);
            }
        };

        vm.pauseClock = function () {
            $interval.cancel(vm.clockId)
            vm.clockId = 0;
        };
        this.stopClock = function () {
            $interval.cancel(vm.clockId)
            vm.seconds = 0;
            vm.minutes = 0;
            vm.hours = 0;
            vm.clockId = 0;
        };
    }
})(angular)