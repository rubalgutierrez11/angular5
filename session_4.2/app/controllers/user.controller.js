(function (angular) {
    'use strict';

    angular
        .module('myApp.controllers')
        .controller('userController', Definition);

    Definition.$inject = ['userService', '$rootScope'];

    function Definition(userService, $rootScope) {
        var vm = this;
        vm.isLogged = false;
        vm.login = function (userName, pass) {
            vm.isLogged = userService.login(userName, pass);
            // $rootScope.$broadcast('OnLoginEvent', {
            //     isLogged: vm.isLogged
            // });
            if (!vm.isLogged) {
                alert('Invalid User');
            }
            $rootScope.$broadcast('OnLoginEvent', {});
        };

        vm.logOut = function () {
            vm.isLogged = false;
            userService.logOut();
            $rootScope.$broadcast('OnLoginEvent', {});
        };
    }
})(angular)