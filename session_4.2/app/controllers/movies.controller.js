(function (angular) {
    'use strict';

    angular
        .module('myApp.controllers')
        .controller('moviesController', Definition);

    Definition.$inject = ['$q', 'moviesService', 'userService', '$rootScope'];

    function Definition($q, moviesService, userService, $rootScope) {
        var vm = this;
        vm.showLoading = false;
        vm.movies = [];

        vm.search = function (textToSearch) {
            vm.showLoading = true;
            moviesService
                // .getPage(1, textToSearch) no paging for now
                .getPage(textToSearch)
                .then(function (movies) {
                    vm.movies = movies;
                    vm.showLoading = false;
                });
        };

        $rootScope.$on('OnLoginEvent', function (event, args) {
            vm.isLogged = userService.isLogged();
            vm.userName = userService.getName();
        });
    }
})(angular)