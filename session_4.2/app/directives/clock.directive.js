(function (angular) {
    'use strict';

    angular
        .module('myApp.directives')
        .directive('myClockDirective', Definition);

    function Definition() {
        return {
            controller: 'clockController',
            controllerAs: 'vm',
            scope: {
            },
            templateUrl: 'app/templates/clock.html'
        }
    }
})(angular)


