(function (angular) {
    'use strict';

    angular
        .module('myApp.directives')
        .directive('moviesDirective', Definition);

    function Definition() {
        return {
            controller: 'moviesController',
            controllerAs: 'vm',
            scope: {
            },
            templateUrl: 'app/templates/movies.html'
        }
    }
})(angular)


