(function (angular) {
    'use strict';

    angular
        .module('myApp.directives')
        .directive('userDirective', Definition);

    function Definition() {
        return {
            controller: 'userController',
            controllerAs: 'vm',
            scope: {
            },
            templateUrl: 'app/templates/user.html'
        }
    }
})(angular)


