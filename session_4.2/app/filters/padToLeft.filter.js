(function (angular) {
    'use strict';

    angular
        .module('myApp.filters')
        .filter('padToLeft', Definition);

    function Definition() {
        return function (value) {
            return ('00' + value).slice(-2);
        }
    }
})(angular)

