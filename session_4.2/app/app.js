(function (angular) {
    'use strict';

    angular
        .module('myApp', [
            'ui.router',
            'myApp.services',
            'myApp.controllers',
            'myApp.directives',
            'myApp.filters'
        ])
        .config(function ($stateProvider, $locationProvider) {
            var clockState = {
                name: 'movies',
                url: '/movies',
                templateUrl: 'app/templates/moviesView.html'
            };
            var moviesState = {
                name: 'clock',
                url: '/clock',
                templateUrl: 'app/templates/clockView.html'
            };
            $stateProvider.state(clockState);
            $stateProvider.state(moviesState);
        });
})(angular);

(function (angular) {
    'use strict';

    angular.module('myApp.services', []);
})(angular);

(function (angular) {
    'use strict';

    angular.module('myApp.controllers', []);
})(angular);

(function (angular) {
    'use strict';

    angular.module('myApp.directives', []);
})(angular);

(function (angular) {
    'use strict';

    angular.module('myApp.filters', []);
})(angular); 