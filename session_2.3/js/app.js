var router = new Router({
    '': {
        template: `
        <article data-component="clock"></article>
        <article data-component="clock"></article>
        <article data-component="greet"></article>`,
        components: ['clock', 'greet']
    },
    'clock': {
        template: `
        <article data-component="clock"></article>
        <article data-component="clock"></article>`,
        components: ['clock']
    },
    'greet': {
        template: `
        <article data-component="greet"></article>
        <article data-component="greet"></article>`,
        components: ['greet']
    },
}
);

//var m = new Module({moduleName:`app`, components:['clock','greet']});
/*
cual es la definicion de hoisting: es mover las declaraciones de js hasta arriba.

diferencias entre let var y const: const no se puede reasignar una variable, let se usa cuando debo reasignar variables y var es simple definicion de una variable en js.

los operadores nuevos en es6

un objeto no modificable

try catch leo los componenets que estan se cea uno y se elimina component


*/