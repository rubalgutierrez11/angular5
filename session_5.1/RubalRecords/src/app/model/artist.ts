export class Artist {
    group: string    
    year: string
    genre: string
    country: string
}