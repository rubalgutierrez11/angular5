import { Component } from '@angular/core';
import { Artist } from './model/artist'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  myModel: any;

  clickMessage = '';

  onClickMe() {
    this.clickMessage = 'You are my hero!';
  }
  
 albums: Artist[] = [
    {
      group: 'MyBand1',      
      year: '1999',
      genre: 'Pop',
      country: 'UK'
    },
    {
      group: 'MyBand2',      
      year: '1982',
      genre: 'Rock',
      country: 'USA'
    }
  ];
  
  results: Artist[] = [];
  searchText: string;

  Search() {    
    this.results = this.albums;    
  };
  
}






