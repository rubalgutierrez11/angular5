
## Practica Client
	Archivo: index.html
	Contiene la practica de cliente en un banco para hacer depositos y retiros,  validacion de inputs, y tambien el ejemplo de 'use strict'.

## Practica Encriptor
	Archivo: encrypt.html
	Especificaciones:
	El constructor "Encriptor" deberá tener dos métodos públicos, encriptar y desencriptar
	Cada método recibirá un parámetro.
	El parámetro del constructor Encriptor sera un número.
	*	Será necesario que sea únicamente un numero entero.
	*	Cualquier otro caracter no numérico arrojará un error.
	*	Será un entero positivo.

	El parámetro de cada método será un string: 
	*	Contendrá caracteres unicamente alfabéticos incluyendo mayúsculas y minúsculas. 
	*	Se debe respetar el alfabeto español incluyendo la letra "ñ".
	*	Cualquier tipo de acentos o diéresis serán aceptados pero serán ignorados utilizando únicamente la letra base.
	*	Cualquier otro caracter no valido será rechazado por el método.
	*	el numero no será mayor a la cantidad de caracteres del alfabeto español.

	El método encriptar regresará un string  al cual le habrá aplicado al parametro un proceso de desplazamiento del abecedario una cantidad de digitos indicado por el parametro del constructor.


	El método desencriptar regresará un string  al cual le habrá aplicado al parametro un proceso de desplazamiento del abecedario una cantidad de digitos indicado por el parametro de manera inversa al metodo "encriptar".

## Wikipedia
	Archivo: wikipedia.html
	Contiene la practica de html5 de hacer una pagina tipo wiki 
