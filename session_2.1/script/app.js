//Temas vistos en la primera parte de la session 2
//console.log("test");

// class Auto{

//     constructor(nombre){
//         this.nombre=nombre;
//     }
//     run(){
//         console.log('runnning ' + this.nombre +' ');
//     } 
// }

// var BMW = new Auto ("BMW");
// //BMW.run();

// // const no lo podemos inicializar mas de 1 vez.

// // const a = { prop1 : 1, prop2:2};

// // a.prop1= 2;

// //a = { prop11 : 1, prop21:2}; error

// // tarea buscar use strict
// function b(){
//     "use strict";
//  var c=2;
//     console.log(c);
// }
// //console.log(c);

// //b();

// var [a,b,c,d] = [1,2,3,4];
// [a,b,c,d] = [d,c,b,a];
// //descructuring array
// console.log(a,b,c,d);

//es6 new features

var {a}= {a:"hello"};
console.log(a);

//object matching