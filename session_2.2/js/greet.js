class Greet extends Component {
    constructor() {
        super({
            selector:'[data-component="greet"]',
            template: `
                    <article class="Mirror">        
                    <input  data-value="nameInput"></input>
                    <h1>Hello my name is <span class="hours" data-value="nameOutput"></span></h1>
                   </article>`,
            data: {},
            methods:{}
        });        
    }
}


// (function () {
//     var scope = {
//         container: document.querySelector('[data-module="app"]'),
//         init() {
//             this.container.innerHTML = this.template;
//             this.render();
//         },
//         render() {
//             this.mount();
//         },
//         mount() {

//             var i = document.querySelector('[data-value="nameInput"]');
//             var c = document.querySelector('[data-value="nameOutput"]');
//             console.log(i);
//             console.log(c);

//             i.addEventListener('keyup', function () {
//                 c.innerHTML = i.value;
//             });
//         },
//         template: `
//         <article class="Mirror">        
//         <input  data-value="nameInput"></input>
//         <h1>Hello my name is <span class="hours" data-value="nameOutput"></span></h1>
//        </article>`,
//     }
//     scope.init();
// })();