class ChkBoxChecked extends Component {
    constructor() {
        super({
            selector:`[data-component="ischecked"]`,
            template: `
                    <article class="IsChecked">        
                    <input type="checkbox" ></input>                    
                    <span class="labelChecked" data-value="chkbox"></span>
                   </article>`,
            data: {chkbox:false},
            methods:{}
        });        
    }
}
