
class Component {
    constructor(structure = {
        selector: '',
        template: '',
        data: {},
        methods: {}
    }) {
        try {
            this.selector = structure.selector;
            this.container = document.querySelector(structure.selector);
            this.template = structure.template;
            this.methods = structure.methods;
            this.data = this.buildData(structure.data);
            //console.log(this.data);
            this.mount();
        }
        catch (e) {
            console.log(e);
        }
    }

    buildData(data = {}) {
        var computedInitailData = {}
        this._data = data;

        for (const propertyName in data) {
            if (data.hasOwnProperty(propertyName)) {
                Object.defineProperty(computedInitailData, propertyName, {
                    set: function (x) {
                        this._data[propertyName] = x;
                        var toChange = this.container.querySelectorAll(`[data-value="${propertyName}"]`);
                        if (toChange) {
                            toChange.forEach(element => {
                                //this.render(toChange, data[propertyName]);
                                this.render(element, data[propertyName]);
                            });

                        }
                    }.bind(this),
                    get: () => (this._data[propertyName])
                });
            }
        }
        return computedInitailData;
    }

    render(element = document.querySelector(this.selector),
        content = this.template) {
        if (Node.prototype.isPrototypeOf(element)) {
            if (element.tagName == 'Input') {
                element.value = content;
            }
            else {
                element.innerHTML = content;
            }

        }

        //  if (NodeList.prototype.isPrototypeOf(element)) {
        //     element.forEach(x => {
        //         //this.render(toChange, data[propertyName]);
        //         this.render(x);
        //     });
        //  }
    }

    mount() {
        //montas el template
        this.render(this.container, this.template);

        //montas los valores de los controles
        for (const key in this._data) {
            if (this._data.hasOwnProperty(key)) {
                var toChange = this.container.querySelector(`[data-value="${key}"]`);
                //console.log(toChange);
                if (toChange) {
                    if (toChange.tagName == 'INPUT') {
                        c.value = this._data[key];
                    }
                    else {
                        toChange.innerHTML = this._data[key];
                    }
                }
            }

        }

        for (const methodName in this.methods) {
            console.log(methodName);
            var c = document.querySelector('[data-event-click="' + methodName + '"]');
            if (c) {
                c.addEventListener('click', this.methods[methodName]);
            }
        }


    }
}
// (function () {
//     var scope = {
//         container: document.querySelector('[data-module="app"]'),
//         init() {
//             this.container.innerHTML = this.template;
//             this.render();
//         },
//         render() {
//             this.mount();
//         },
//         mount() {

//             for (const propertyName in scope.methods) {
//                 var c = document.querySelector('[data-event-click="' + propertyName + '"]');
//                 if (c) {
//                     c.addEventListener('click', scope.methods[propertyName]);
//                 }
//             }

//             var dataPropertyNames = Object.propertyNames(this.data);
//             //console.log(dataPropertyNames);
//             for (let i = 0; i < dataPropertyNames.length; i++) {
//                 var o = document.querySelector('[data-values="' + dataPropertyNames[i] + '"]');
//                 if (o) {
//                     o.innerHTML = this.data[dataPropertyNames[i]];
//                 }

//             }
//         },
//         template: `
//     <article class="reloj">
//         <section class="display">
//         <span class="hours" data-values="hours">:</span>   
//         <span class="minutes" data-values="minutes">:</span>
//         <span class="seconds" data-values="seconds"></span>
//         </section>
//         <section class="controls">
//             <button data-event-click="startClock">Start</button>
//             <button data-event-click="pauseClock">Pause</button>
//             <button data-event-click="resetClock">Reset</button>
//         </section>
//        </article>`,

//         data: { seconds: 0, minutes: 0, hours: 0 },
//         methods: {
//             updateClock() {
//                 scope.data.seconds++;
//                 if (scope.data.seconds > 59) {
//                     scope.data.seconds = 0;
//                     scope.data.minutes++;
//                 }
//                 if (scope.data.minutes > 59) {
//                     scope.data.minutes = 0;
//                     scope.data.hours++;
//                 }
//             },
//             startClock() {
//                 if (!scope.data.clockId) {
//                     scope.data.clockId = setInterval(function () {
//                         scope.methods.updateClock();
//                         scope.render();
//                     }, 1000);
//                 }
//             },
//             resetClock() {
//                 clearInterval(scope.data.clockId);
//                 scope.data.hours = 0;
//                 scope.data.minutes = 0;
//                 scope.data.seconds = 0;
//                 scope.data.clockId = 0;
//                 scope.render();

//             },
//             pauseClock() {
//                 clearInterval(scope.data.clockId);
//                 scope.data.clockId = 0;
//             }
//         }


//     }
//     scope.init();
// })();


// (function () {
//     var scope = {
//         container: document.querySelector('[data-module="app"]'),
//         init() {
//             this.container.innerHTML = this.template;
//             this.render();
//         },
//         render() {
//             this.mount();
//         },
//         mount() {

//             var i = document.querySelector('[data-value="nameInput"]');
//             var c = document.querySelector('[data-value="nameOutput"]');
//             console.log(i);
//             console.log(c);

//             i.addEventListener('propertyNameup', function () {
//                 c.innerHTML = i.value;
//             });
//         },
//         template: `
//         <article class="Mirror">        
//         <input  data-value="nameInput"></input>
//         <h1>Hello my name is <span class="hours" data-value="nameOutput"></span></h1>
//        </article>`,
//     }
//     scope.init();
// })();